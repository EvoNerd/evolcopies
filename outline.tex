\documentclass[12pt]{amsart}
\usepackage{geometry}                % See geometry.pdf to learn the layout options. There are lots.
\geometry{letterpaper}                   % ... or a4paper or a5paper or ... 
%\geometry{landscape}                % Activate for for rotated page geometry
%\usepackage[parfill]{parskip}    % Activate to begin paragraphs with an empty line rather than an indent
\usepackage{graphicx}
\usepackage{amssymb}
\usepackage{epstopdf}
\DeclareGraphicsRule{.tif}{png}{.png}{`convert #1 `dirname #1`/`basename #1 .tif`.png}

\title{Brief technical note about our awesome, catchily-named model}
\author{Hermina Ghenu \& Ben Bolker}
%\date{}                                           % Activate to display a given date or no date

\begin{document}
\maketitle

\section{Background}
\subsection{here's a problem that people care about}
\begin{itemize}
	\item Evolution of discrete character states that are ordered and evolution goes sequentially from 0 to 1 to 2, etc. (or 2 to 1 to 0, etc.)
	\item Continuous-time Markov process, more specifically a time-homogeneous birth-death process
	\item We are interested specifically in models where the probability of an event is state dependent: the transition probability from the current state to the next state is the product of the rate and the current state's value
	\item We are interested in an evolutionary model so we have a phylogeny where we assume that observed characters are only on the tips
	\item The birth-death process is time-homogeneous along a branch but different branches can have different rates
	\item \ldots Examples of people who care (or might care) about these models\ldots
\end{itemize}

\subsection{here's how these models generally work}
\begin{itemize}
	\item Bailey 1964 described these kinds of birth-death processes
	\item more detail on transitions among states (?)
	\item there is no stationary distribution for these types of models: since 0 is an absorbing state, if death$\geq$birth then will go to $0$. If birth$>$death then it will either go to +Inf or 0 (\textit{I need to check these statements with Bailey 1964}).
	\item Use pruning algorithm to calculate the likelihood of parameter along the tree (Felsenstein 1981)
	\item However, pruning algorithm requires that you know the prior probabilities of the characters so that the root node can be marginalized out (``Pulley Principle'', Felsenstein 1981). Usually (i.e. in nucleotide substitution models) people use the stationary distribution of the model as the prior distribution; this assumes that there is an evolutionary steady-state. In our case there is no stationary distribution! Naively one might think that we will be okay because we're \emph{not} doing a tree search, we're just finding the ML rate for a single given tree so we don't have to worry about comparing ML values among different trees. But the prior probability does matter: models will make biologically nonsensical predictions about the ancestral state at the root.
\end{itemize}

\subsection{other people have implemented this before}
\subsubsection{CAFE: Han 2013, Hahn 2007, Hahn 2005}
\begin{description}
	\item [how does it work?] Uses Bailey closed form solution(s) to get the probability of observing change of state $s \rightarrow c$. Only has 2 evolutionary models: birth=death or birth$\ne$death (no models with innovation). A single tree is used for all genes to do pruning algorithm. The maximum number of copies is set to 2.5x the largest value observed in the data. The prior probability of each state at the root $=P0(\bar{x}+1)$, where $\bar{x}$ is the mean of all non-zero copy numbers across all gene families and leaves. In the original CAFE (v1), where birth=death and there's no heterogeneity among lineages, the prior distribution at the root was discrete uniform with $probability($0 at root$)=0$. The `likelihood' for each gene family = probability of maximum \textit{a posteriori} ancestral state $\cdot$ prior probability of this ancestral state. Uses the triangle hill-climbing algorithm thingy to optimize. The likelihood calculation for each gene family can be parallelized to speed up the whole calculation. After the rate is estimated it can also give a `p-value' of how likely each of the observed copy numbers is. It does this by starting from the MAP ancestral state at the root and doing a forward tree traversal to get the probability of the observed extant states. Newest version also takes into account error in copy number estimates from high-throughput sequencing.
	\item [what do we know it does wrong?] The probability calculation is numerically unstable at large state values. The optimization algorithm is pretty slow.
	\item [how generalizable/flexible?] Not very, basically just made for out-of-the-box use.
	\item [what outputs can it give?] only 2 outputs relevant to us, MLE of rate(s) \& MAP ancestral state at root for each gene family.
	\item [how widely used?] People really like the p-value thing because it can be used to identify gene families whose copy numbers are evolving especially slowly/quickly. I've found something like 27 studies that used CAFE for this purpose vs. only 3 studies that used CAFE to look at rates of evolution on the tree (Hahn 2007 in primates, Hahn 2007 in flies, Parrent 2009) \textit{check to see if there are any more}.
\end{description}

\subsubsection{BEGFE: Liu 2011}
\begin{description}
	\item [how does it work?] This is just a Bayesian MCMC version of the original CAFE (v1). So it estimates the posterior distribution for a single birth=death rate, i.e.\ the rate is uniform across the given tree. A discrete uniform distribution is used as the prior for internal nodes on the tree; the authors don't say explicitly but I assume that the root is not allowed to have state 0 (this is true in CAFE v1 so I'm assuming also true here).
	\item [what do we know it does wrong?] It also uses the Bailey closed form solution so it probably also has the numerical instability issue.
	\item [how widely used?] haven't been able to find any papers that use this method (i.e.\ to analyze data.)
\end{description}

\subsubsection{DupliPhyML: Ames 2012}
\begin{description}
	\item [how does it work?] 3 models but the relevant one for us is the one with birth=death and innovation. Given the relationships among extant taxa (but \textbf{not} the branch lengths), it gets the MLE of the innovation rate relative to the birth=death rate. It also estimates the branch lengths for the tree, I guess in units of `events.' This model also allows heterogeneity among gene families by estimating the $\alpha$ shape parameter for a discrete $\Gamma$-distribution with 4 classes (I think $\beta=\alpha$ so that the mean of the distribution is 1). Instead of putting a prior distribution on the root, the stationary distribution of the Markov chain is estimated and used to get the likelihood (when innovation$>$0 there is no absorbing state so the stationary distribution exists\ldots \textit{Actually, I'm really confused about this because eqn (3) shows that the probability of an innovation event is 0 and the text states that there is a sink state at 0. So does this mean that the stationary distribution they use is just some dumb thing where the max copy number, in this case 75, is the most likely value?!}). Finally, it uses the method of Felsenstein 1992 to account for gene families that were not observed in any of the extant taxa.
	
	It works by using ML to estimate the branch lengths, relative innovation rate, and shape parameter in a way that is analogous to ML tree estimation (when the relationships among taxa are given). The maximum number of copies is set to 75. Taylor expansion is used to estimate the matrix exponential in order to get the probability along a branch of length $t$. And the stationary distribution is estimated by multiplying an arbitrary starting vector by a probability matrix until a stable distribution is reached.
	\item [how generalizable/flexible?] It does include 2 other models, but for those the probability of an event is \textbf{not} state dependent. Otherwise, it's not very flexible.
	\item [how widely used?] haven't been able to find any papers that use this method (i.e.\ to analyze data.)
\end{description}

\subsubsection{BadiRates: Librado 2012}
\begin{description}
	\item [how does it work?] 5 models but 4 are relevant for us: birth=death, birth=death with innovation, birth$\ne$death, birth$\ne$death with innovation. Uses ML or MAP. Allows rates to differ among branches (i.e.\ lineage heterogeneity). User can specify the prior distributions on the parameters; it's not clear to me what the options are other than changing the $\alpha$ shape parameter of the $\Gamma$-distribution but I guess this is for the MAP method anyway. Like CAFE, after the rate is estimated it can also try to identify genes whose copy numbers are evolving unusually slowly/	quickly. Like DupliPhyML, I think that the models can allow heterogeneity among gene families and account for gene families that were not observed in any of the extant taxa.
	
	\textit{I need to read Csuros \& Miklos 2009 in order to understand the model assumptions and how these differ from Bailey 1964.}
	\item [what do we know it does wrong?] ?
	\item [how generalizable/flexible?] Can specify root prior distribution, compare 4 models, change the maximum number of copies, 
	\item [what outputs can it give?] similar to CAFE.
	\item [how widely used?] I found 3 papers, 2 (Wang 2014, Zwick 2012) of which used the program to identify unusually evolving gene families and the last (Librado \& Rozas 2013) where it is unclear how BadiRates was used.
\end{description}

\section{Findings}
\subsection{technical details of our method}
What do we do differently than other people?
\begin{itemize}
	\item Multiple models of copy number evolution that can be compared directly
	\item Put a peaked prior on the root (the same as CAFE), but we give you the option of changing the prior too
	\item Don't use the numerically unstable Bailey solution, instead do brute-force matrix exponentiation. We've found this to be numerically stable, therefore better than Bailey.
	\item Implemented this in R so it's easy to do things like get confidence intervals
\end{itemize}

\subsection{how our model performs}
\begin{itemize}
	\item some simulations under a variety of freaky conditions
	\item some real examples
\end{itemize}

\section{Conclusion}
\subsection{brief summary}
\subsection{potential implications}

\end{document}  