# README #

An R package to estimate parameters of evolutionary models for discrete-state, continuous-time models.

To install: 

```
library("devtools")
install_bitbucket("EvoNerd/evolcopies/birthdeath",auth_user=...,password=...)
```