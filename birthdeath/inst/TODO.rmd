## Stuff to do/consider

<<<<<<< HEAD
* HG: add examples that actually do the MLE fitting
=======
* HG: check licenses on example data
* HG: clean up documentation
* add examples that actually do the MLE fitting
>>>>>>> 46401bacf4fa2c8514914b44f447edca727416f6
* (maybe add `bbmle` as Suggests or Imports?)
* write S3 methods for "model" objects? compare depmixS4 package (eval/uneval model)
* build in rate heterogeneity in a cleaner way
* other kinds of data (e.g. qPCR)
* vignette?

Using `mle2` on a function that takes a parameter vector:
```{r}
x <- 1:5
## optim() likes parameters passed as vectors:
objfun0 <- function(p) {
    -sum(dnorm(x,p[1],p[2],log=TRUE))
}
optim(fn=objfun0,par=c(1,1))$par
## mle2() prefers them as named arguments:
objfun1 <- function(mu,sd) {
    -sum(dnorm(x,mu,sd,log=TRUE))
}
library("bbmle")
coef(mle2(minuslogl=objfun1,start=list(mu=1,sd=1),
          method="Nelder-Mead"))
## if I want to use a vector argument:
## 1. augment the function by telling mle2 what names correspond
##    to the elements of the parameter vector
parnames(objfun0) <- c("mu","sd")
## 2. then use mle2 with vecpar=TRUE
coef(mle2(minuslogl=objfun0,start=list(mu=1,sd=1),
          vecpar=TRUE,
          method="Nelder-Mead"))
```
